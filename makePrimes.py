from time import time
from tqdm import trange, tqdm
import numpy as np
from numpy.lib.format import open_memmap
primes = np.array([])
loaded = False


def savefile(n):
    duration = 10
    filename = "primes.np"
    primes = open_memmap(filename=filename, mode='w+',dtype=np.bool, shape=(n,))
    for index, composite in tqdm(enumerate(primes), total=int(n**0.5)):
        if index < 2: continue
        if not composite:
            start = time()
            if duration < 1: iterr = range(index**2, n, index)
            else: iterr = trange(index**2, n, index)
            for number in iterr:
                primes[number] = True
            duration = time() - start
        if index**2 > n: break
    primes[0] = True
    primes[1] = True
    primes.flush()

def loadfile():
    global primes, loaded
    filename = "primes.np"
    primes = open_memmap(filename, mode='r', dtype=np.bool)
    loaded = True

def isPrime(n):
    assert loaded
    return not primes[n]

def primesfrom(start, stop=None):
    if stop is None:
        start, stop = 2, start
    for i in range(start, stop):
        if isPrime(i): yield i


if __name__ == "__main__":
    from sys import argv
    if len(argv) > 1:
        top = eval(argv[1])
    else:
        top = 10**5
    savefile(int(top))
    print('\n\n')
else:
    loadfile()
