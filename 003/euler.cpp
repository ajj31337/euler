#include <iostream>
#include <set>
#include <string>
using namespace std;

void factor(long long num, set<long>& facts)
{
//	cout << num << "\n\n";
	for(int i = 2; i*i < num+1; i++){
		if( num%i == 0 ){
//			cout << "===" << num << '/' << i << '\n';
			int f = num / i;
			facts.insert(i);
			factor(f, facts);
			return;
		}
	}
//	cout << '-' << num << "-\n";
	facts.insert(num);
	return;
}
int main(int argc, char *argv[])
{
	long test;
	if( argc > 1) {
		test = stol(argv[1]);
		cout << "Factoring " << test << '\n';
	}
	else {
		test = 58;
	}
	set<long> factors;
	factor( test, factors);


	for( auto elem : factors ){
		cout << elem << '\n';
	}
	cout << "\n\n" << *factors.rbegin() << '\n';
}
