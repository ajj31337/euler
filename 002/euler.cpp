#include <iostream>
using namespace std;

void fiba(int& a, int& b)
{
	int n = a + b;
	a = b;
	b = n;
}

int main()
{
	int limit = 4000000;
	int a = 1, b = 2;
	long long sum = 0;
	while( a < limit ){
		if (a%2 == 0) sum += a;
		fiba(a,b);
	}
	cout << sum << '\n';
}
