all: build run

build: euler.cpp
	g++ euler.cpp

run: build
	./a.out
