#include <iostream>
#include <cmath>

#define True  (1)
#define False (0)

using namespace std;

bool isPan(int);
int firstDigit(int);
/*
int main()
{
	isPan(987654321);
	return 0;

}
*/
/*
int main()
{
	int top = 0;
	int v;
	for( int i = 100; i < 1000; i++ )
	{
		for( int j = 100; j < 1000; j++ )
		{
			v = i * j;
			if( v > top ){
				if( isPan(v) ){
					top = v;
			}}
		}
	}
	cout << top << '\n';
}
*/
int main()
{
	int x;
	while True
	{
		cout << "Digit: ";
		cin >> x;
		cout << firstDigit(x) << '\n';
	}
}

int firstDigit(int value)
{
	while( value > 9 ){
		value = value / 10;
	}
	return value;
}

bool isPan(int value)
{
	int last;
	int first;
	int top;
	while( value > 9 ){
		last  = value % 10;
//		first = value / (10 * (int)(log10((float)value)) );
		first = firstDigit(value);
		if( first != last ) return False;
		top = first * pow(10.0, (int)log10(value) );
//		cout << first << ' ' << value << ' ' << last << "\n  " << top << '\n';
		value = value - top;
		value = value / 10;
	}
	return True;
}
