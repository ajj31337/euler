#include <iostream>
using namespace std;

int main()
{
	long long target = 1000000000000;
	long long b = 15;
	long long n = 21;
	while( n < target )
	{
		long long bt = 3*b + 2*n - 2;
		long long nt = 4*b + 3*n - 3;
		b = bt;
		n = nt;
	}
	cout << "There are " << b << " blues\n";
	return 0;
}
