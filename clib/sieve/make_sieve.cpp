#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string>

#define True 1
#define False 0

using namespace std;

bool* make_sieve(int);

int main(int argc, char** argv)
{
	bool verbose = False;
	int size;
	if (argc > 1) { size = stoi(argv[1]); }
	else { size = 0; verbose = True; }
	
	while( size < 10 )
	{
		cout << "Sieve size: ";
		cin >> size;
	}
	bool* sieve = make_sieve(size);
	
	if( verbose )
	{
	for( int i = 0; i < size; i++)
		{
			cout << i << ": ";
			if (sieve[i]) cout << "Prime\n";
			else cout << "Not Prime\n";
		}
	}
	else
	{
		FILE * f = fopen("primes.bin", "wb");
		fwrite( sieve, sizeof sieve[0], size, f );
		fclose(f);
	}
}

bool* make_sieve(int size)
{
	bool* sieve = new bool[size];
	for( int i = 0; i < size; i++)
	{
		sieve[i] = True;
	}
	sieve[1] = False;
	for( int i = 2; i*i < size; i++ )
	{
		if( sieve[i] )
		{
			for( int j = i*2; j < size; j += i )
			{
				sieve[j] = False;
			}
		}
	}
	return sieve;
}
